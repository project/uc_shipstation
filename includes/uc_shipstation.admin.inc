<?php
/**
 * @file
 *
 * Admin settings page.
 */

/**
 * Implements hook_admin_page().
 *
 * Provides an interface for configuring module options.
 */
function uc_shipstation_admin_page() {
  $form = array();

  // ShipStation username.
  $form['uc_shipstation_username'] = array(
    '#type' => 'textfield',
    '#title' => t('ShipStation Username'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_shipstation_username', ''),
  );

  // ShipStation password.
  $form['uc_shipstation_password'] = array(
    '#type' => 'password',
    '#title' => t('ShipStation Password'),
    '#default_value' => variable_get('uc_shipstation_password', ''),
    '#attributes' => array('autocomplete' => 'off'),
  );

  // ShipStation logging.
  $form['uc_shipstation_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log requests to ShipStation'),
    '#description' => t('If this is set, all API requests to ShipStation will be logged to Drupal watchdog.'),
    '#default_value' => variable_get('uc_shipstation_logging', 0),
  );

  // ShipStation reload.
  $form['uc_shipstation_reload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reload all orders to ShipStation'),
    '#description' => t('If this is set, on API endpoint request all orders will be returned.'),
    '#default_value' => variable_get('uc_shipstation_reload', 0),
  );

  // ShipStation alternate authentication.
  $form['uc_shipstation_alternate_auth'] = array(
    '#type' => 'textfield',
    '#title' => t('Alternate Authentication'),
    '#description' => t('Use this field if your web server uses CGI to run PHP.'),
    '#default_value' => variable_get('uc_shipstation_alternate_auth', ''),
  );

  // ShipStation export paging.
  $form['uc_shipstation_export_paging'] = array(
    '#type' => 'select',
    '#title' => t('Number of Records to Export per Page'),
    '#description' => t('Sets the number of orders to send to ShipStation at a time. Change this setting if you experience import timeouts.'),
    '#options' => array(20 => 20, 50 => 50, 75 => 75, 100 => 100, 150 => 150),
    '#default_value' => variable_get('uc_shipstation_export_paging', '100'),
  );

  // Order notes to import.
  $form['uc_shipstation_order_notes_field'] = array(
    '#type' => 'select',
    '#title' => t('Field used for order notes'),
    '#required' => FALSE,
    '#description' => t('Choose a field you use for admin order notes (attached to Ubercart Order entity).'),
    '#options' => _uc_shipstation_load_field_options(),
    '#default_value' => variable_get('uc_shipstation_order_notes_field', ''),
  );

  // ShipStation order export status.
  $form['uc_shipstation_export_status'] = array(
    '#type' => 'select',
    '#title' => t('Order Status to Export into ShipStation'),
    '#required' => TRUE,
    '#options' => uc_order_status_options_list(),
    '#default_value' => variable_get('uc_shipstation_export_status', ''),
    '#multiple' => TRUE,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  $form['#submit'][] = 'uc_shipstation_admin_page_submit';

  return $form;
}

/**
 * Submit handler for the admin settings form.
 *
 * This submit handler is basically a copy of system_settings_form_submit
 * handler. The reason we aren't using the system_settings_form_submit handler,
 * is because we need to do something special with one of the fields (password)
 * before we store it into the variables table.
 */
function uc_shipstation_admin_page_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    if ($key == 'uc_shipstation_password') {
      if (!empty($value)) {
        variable_set($key, $value);
      }
    } else {
      variable_set($key, $value);
    }
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Builds a list of all fields available on the site.
 */
function _uc_shipstation_load_field_options() {
  $options = array();
  $options['None'] = t('None');
  foreach (field_info_fields() as $field_name => $field) {
    $options[$field_name] = $field_name;
  }
  return $options;
}