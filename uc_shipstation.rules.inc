<?php
/**
 * @file
 *
 * Declares rules events, actions, and conditions for Ubercart Shipstation module.
 */


/**
 * Implements hook_rules_event_info().
 */
function uc_shipstation_rules_event_info() {
  $events = array();

  $events['uc_shipstation_order_exported'] = array(
    'label' => t('Order has been exported to ShipStation'),
    'group' => t('Ubercart ShipStation'),
    'variables' => array(
      'order' => array(
        'label' => t('Order', array(), array('context' => 'a Drupal Ubercart order')),
        'type' => 'uc_order',
        'skip save' => TRUE,
      ),
    ),
  );

  $events['uc_shipstation_order_success'] = array(
    'label' => t('ShipStation reports an order has completed successfully'),
    'group' => t('Ubercart ShipStation'),
    'variables' => array(
      'order' => array(
        'label' => t('Order', array(), array('context' => 'a Drupal Ubercart order')),
        'type' => 'uc_order',
        'skip save' => TRUE,
      ),
      'tracking_number' => array(
        'type' => 'text',
        'label' => 'Shipping tracking number',
      ),
      'carrier' => array(
        'type' => 'text',
        'label' => 'Shipping carrier',
      ),
      'service' => array(
        'type' => 'text',
        'label' => 'Shipping service',
      ),
    ),
  );

  return $events;
}
